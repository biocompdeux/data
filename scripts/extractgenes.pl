#!/usr/bin/perl
use strict;

sub LoadGenes;
sub CreateGene;

my $numargs = $#ARGV + 1;
if ($numargs != 2) {
  print "\nUsage: extractgenes.pl [inputfile] [outputfile]\n";
  exit;
}

my $inputfile = $ARGV[0];
my $outputfile = $ARGV[1];

if (-e $inputfile) {
  my $result = LoadGenes($inputfile);
  unless (open OUTPUT, '>'.$outputfile) {
    die "\nUnable to create '$outputfile'\n";
  }
  print OUTPUT $result;
  close OUTPUT;
}


sub LoadGenes($) {
  my $genes;
  my $file = $_[0];
  open(IN, $file) or die "Unable to open file: $file\n";
  my $foundFeatures = 0;
  my $foundCDS = 0;
  my $foundName = 0;
  my $foundTranslation = 0;
  my ($gene, $product, $location, $accession, $aa);

  while (defined(my $line = <IN>)) {
    $line =~ s/^\s+|\s+$//g; # remove whitespace from line
    if ($line =~ /^ACCESSION[\s]*([\s\S]*)/) {
      $accession = $1;
    }
    elsif ($line =~ /^VERSION[\s]*(\w+[.][\w])[\s]*(GI:[\s\S]*)/) {
      $gene = $2;
    }
    elsif ($gene ne '' && $line =~ /^FEATURES/) {
      $foundFeatures = 1;
    }
    elsif ($gene ne '' && $foundFeatures && $line =~ /^CDS/) {
      $foundCDS = 1;
    }
    elsif ($gene ne '' && $foundFeatures && $foundCDS && $product eq '' && $line =~ /\/product=\"([\s\S]+)\"/) {
      $product = $1;
    }
    elsif ($gene ne '' && $foundFeatures && $foundCDS && $line =~ /\/translation=\"(\w+)/) {
      $foundTranslation = 1;
      $aa = $1;
      $aa .= ReadAA();
    }
    elsif ($gene ne '' && $foundFeatures && $line =~ /\/map=\"([\s\S]+)\"/ ) {
      $location = $1;
    }
    elsif ($line =~ /^\/\//) { # end of gene info
      $genes .= CreateGene($gene, $product, $location, $accession, $aa);
      $gene = ""; $product = ""; $location = ""; $accession = ""; $aa = "";
      $foundFeatures = 0; $foundCDS = 0; $foundName = 0; $foundTranslation = 0;
    }
  }
  close(IN);
  return $genes;
}

sub ReadAA {
  my $restAA = "";
  while (defined(my $line = <IN>)) {
    $line =~ s/^\s+|\s+$//g; # remove whitespace from line
    if ($line =~ /([A-Z]+)/g) {
      $restAA .= $1;
    }
    if ($line =~ /\"/) {
      last;
    }
  }
  return $restAA;
}

sub CreateGene($$$$$) {
  my ($gene, $product, $location, $accession, $aa) = @_;
  return "$gene|$product|$location|$accession|$aa\n";
}
