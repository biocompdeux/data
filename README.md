# README #

This is the DATA layer for the Biocomputing II project.

### What is this repository for? ###

* Scripts to extract gene info from GenBank file
> /data/scripts/extractgenes.pl: extract gene summary from GenBank file

* Data from extracted GenBank file
>  (/data/chr3genes.txt): GI:XXXXXX|protein product|location|accession

